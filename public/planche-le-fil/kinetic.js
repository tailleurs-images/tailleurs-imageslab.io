$('#wrapper').kinetic({
    filterTarget: function (target, e) {
        if (!/down|start/.test(e.type)) {
            return !(/area|a|input/i.test(target.tagName));
        }
    }
});

$(".elem").mousedown(function (e) {
    // click on this link will cause ONLY child alert to fire
    e.stopPropagation();
    // stop default action of link
    e.preventDefault();
});


