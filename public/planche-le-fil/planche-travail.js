$(function () {
    $(".elem").draggable();
    $(".elem-text").resizable();


    /* RESIZABLE and preserve ratio img */
    $('.elem-img img').one('load', function () {
        let widthImg = this.width;
        let heightImg = this.height;
        $(this).closest('.elem-img').css("width", widthImg + "px");
        $(this).closest('.elem-img').css("height", heightImg + "px");
        $(this).resizable({
            aspectRatio: widthImg / heightImg
        });
        $(this).closest('.elem-img').append('<button class="button-console"></button>');
    });

    /* Get iun console new styles */
    $(document).on("click", ".button-console", function () {
        let elem = $(this).closest('.elem-img');
        let img = elem.find("img")[0];
        let src = img.src.split("/images/")[1];
        let width = img.width;
        let height = img.height;
        let top = elem.css("top");
        let left = elem.css("left");
        let position = elem.position();
        console.log('<article class="elem-img elem" style="position: relative; top: ' + position.top + 'px; left: ' + position.left + 'px; width: ' + width + 'px; height: ' + height + 'px;"><img src="images/' + src + '"></article>');
    });

    // ZOOM
    $("#sliderzoom").slider(
        {
            value: 100,
            min: 20,
            max: 120,
            slide: function (event, ui) {
                $('#planche').css({ "zoom": ((ui.value) / 100), "-moz-transform": "scale(" + ((ui.value) / 100) + ")", "-moz-transform-origin": "0 0" });
                // console.log(ui.value);
            }
        }
    );


    // LOCALISATION and ZOOM in localStorage
    $("#planche").mousemove(function( event ) {
        let scrollTop = $("#wrapper").scrollTop();
        localStorage.setItem('scrollTop', scrollTop);
        let scrollLeft = $("#wrapper").scrollLeft();
        localStorage.setItem('scrollLeft', scrollLeft);
        let zoom = $("#planche").css("zoom");
        localStorage.setItem('zoom', zoom);
        let handlerZoom = $("#handler-zoom").css("left");     
        localStorage.setItem('handlerZoom', handlerZoom);
    });

    // RESET localisation and zoom
    $(document).on("click", "#reset", function () {
        localStorage.clear();
        window.location.reload();    
    });

    
    




});



$(document).ready(function () {
    
    // Positionning #planche on load
    if(localStorage.getItem('scrollTop')){
        $("#wrapper").scrollTop(localStorage.getItem('scrollTop')).scrollLeft(localStorage.getItem('scrollLeft'));
        $("#sliderzoom").val(5);  
        $("#planche").css("zoom", localStorage.getItem('zoom'));
        $("#handler-zoom").css("left", localStorage.getItem('handlerZoom'));
    }else{
        $("#wrapper").scrollTop($("#repere-onLoad").offset().top).scrollLeft($("#repere-onLoad").offset().left);
    }

    // let count = 0;
    // $(document).bind('mousewheel', function(e){
    //     if(e.originalEvent.wheelDelta /100 > 0) {
    //         console.log('scrolling up !');
    //         count += 1;
    //         let zoomNbr = count;
            
    //         if(zoomNbr < 121){
    //             console.log(zoomNbr);
    //             $('#planche').css({ "zoom": ((zoomNbr) / 100), "-moz-transform": "scale(" + ((zoomNbr) / 100) + ")", "-moz-transform-origin": "0 0" });
    //         }
    //     }
    //     else{
    //         console.log('scrolling down !');
    //         count -= 1;
    //         let zoomNbr = count;
            
    //         if(zoomNbr > 21){
    //             console.log(zoomNbr);
    //             $('#planche').css({ "zoom": ((zoomNbr) / 100), "-moz-transform": "scale(" + ((zoomNbr) / 100) + ")", "-moz-transform-origin": "0 0" });
    //         }
    //     }
    // });
    
});






