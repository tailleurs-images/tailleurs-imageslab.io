/* IMAGES----- */



window.onload = function(){

    let nbrImg = 54;
    let idDiv = 'container-etreinte';
    let pathImg = 'images/photos/IMG_';
    let formatImg = '.JPG'

    let container = document.getElementById('container-etreinte');
    console.log(container);

    for(let i = 0; i < nbrImg; i++){
        // create div hover
        let div = document.createElement("div");
        div.id = "div-" + i;
        div.classList.add("div-hover");
        div.style.width = "calc(100vw / " + nbrImg + ")"
        container.appendChild(div);

        var img = document.createElement("img");
        img.id = "img-" + i;
        img.src = pathImg + i + formatImg;
        container.appendChild(img);
        
        addcss('#div-' + i + ':hover ~ #img-' + i + '{ display: block; }');
    }


 }



 


 function addcss(css){
    var head = document.getElementsByTagName('head')[0];
    var s = document.createElement('style');
    s.setAttribute('type', 'text/css');
    if (s.styleSheet) {   // IE
        s.styleSheet.cssText = css;
    } else {// the world
        s.appendChild(document.createTextNode(css));
    }
    head.appendChild(s);
}



/* SON ----- */


let count = 0;
let pathImg = 'images/photos/IMG_';
let formatImg = '.JPG'

function playSound() {
    son.play();
    count += 1;
    let nbr = getRandomIntInclusive(0, 53);

    console.log(count);

    if(count == 1){
        document.getElementById("button-skip-audio").style.display = "block";
        setTimeout(function(){ displayHover(); }, 90000);
    }   

    if(count != 1){
        createImg(nbr);
        deleteImgTime();
    }   

} 

function stopSound(){
    son.pause();
    displayHover();
}



function displayHover() {
      let etreinteHover = document.getElementById("container-display");
      etreinteHover.style.display="block";
      document.getElementById("button-skip-audio").style.display = "none";
      document.getElementById("button-refresh").style.display = "block";
}

function createImg(nbr){
    let container = document.getElementById("img-container");
    var img = document.createElement("img");
    img.id = "img-" + nbr;
    img.src = pathImg + nbr + formatImg;
    container.appendChild(img);
}
function getRandomIntInclusive(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min +1)) + min;
}

function deleteImgTime() {
    setTimeout(function(){ 
        console.log("delete img");
        let img = document.getElementById("img-container").getElementsByTagName("img")[0];
        img.parentNode.removeChild(img);
     }, 300);
}


